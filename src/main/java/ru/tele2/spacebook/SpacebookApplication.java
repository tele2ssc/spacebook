package ru.tele2.spacebook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class SpacebookApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpacebookApplication.class, args);
	}

}
