package ru.tele2.spacebook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.tele2.spacebook.domain.Place;
import ru.tele2.spacebook.domain.Reservation;

import java.time.LocalDate;
import java.util.Set;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    Reservation findByDateAndSid(LocalDate date, String sid);
    int deleteByDateAndPlace(LocalDate date, Place place);

    @Query("select r from Reservation r left join fetch r.place places where r.date = ?1 and places.location.locationId = ?2")
    Set<Reservation> findByDateAndPlace_Location_LocationIdFetchPlaces(LocalDate date, long locationId);
}
