package ru.tele2.spacebook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tele2.spacebook.domain.Place;

public interface PlaceRepository extends JpaRepository<Place, Long> {
}
