package ru.tele2.spacebook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.tele2.spacebook.domain.Location;

import java.util.List;

public interface LocationRepository extends JpaRepository<Location, Long> {
    List<LocationIdAndName> findAllBy();
    @Query("select l from Location l RIGHT JOIN FETCH l.places places where l.locationId = ?1")
    Location getByIdFetchPlaces(long locationId);
}