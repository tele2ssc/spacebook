package ru.tele2.spacebook.repository;

public interface LocationIdAndName {
    long getLocationId();
    String getName();
}
