package ru.tele2.spacebook.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.tele2.spacebook.domain.Location;
import ru.tele2.spacebook.domain.Place;
import ru.tele2.spacebook.service.LocationService;
import ru.tele2.spacebook.service.PlaceService;

import java.io.IOException;
import java.time.LocalDate;

@Controller
@RequestMapping("/location")
public class LocationController {
    private final PlaceService placeService;
    private final LocationService locationService;

    @Value("${spacebook.images.directory}")
    private String imagesDirectory;


    public LocationController(PlaceService placeService, LocationService locationService) {
        this.placeService = placeService;
        this.locationService = locationService;
    }

    @PreAuthorize("hasAuthority('Role Sharing Editors')")
    @GetMapping("/{currentLocation}/edit")
    public String placeEdit(@PathVariable Location currentLocation, Model model) {
        model.addAttribute("currentLocation", currentLocation);
        model.addAttribute("locationsList", locationService.getLocationsList());
        model.addAttribute("imagesDirectory", imagesDirectory);
        return "editlocation";
    }

    @PreAuthorize("hasAuthority('Role Sharing Users')")
    @GetMapping("/{currentLocationId}")
    public String showLocation(@PathVariable String currentLocationId) {
        return "redirect:/location/" + currentLocationId + "/date/" + LocalDate.now();
    }

    @PreAuthorize("hasAuthority('Role Sharing Users')")
    @GetMapping("/{currentLocationId}/date/{currentDate}")
    public String showLocation(@PathVariable long currentLocationId,
                               @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate currentDate,
                               Model model) {
        model.addAttribute("currentLocation", locationService.getByIdAndReservationDate(currentLocationId, currentDate));
        model.addAttribute("locationsList", locationService.getLocationsList());
        model.addAttribute("imagesDirectory", imagesDirectory);
        return "location";
    }

    @PreAuthorize("hasAuthority('Role Sharing Editors')")
    @PostMapping("/addplace")
    public String addPlace(Place place) {
        placeService.addPlace(place);
        return "redirect:/location/" + place.getLocation().getLocationId() + "/edit";
    }

    @PreAuthorize("hasAuthority('Role Sharing Admins')")
    @GetMapping("/add")
    public String addLocationView() {
        return "addlocation";
    }

    @PreAuthorize("hasAuthority('Role Sharing Editors')")
    @PostMapping("/add")
    public String addLocation(RedirectAttributes redirectAttributes,
                              @RequestParam("name") String name,
                              @RequestParam("backgroundImagePath") MultipartFile img) throws IOException {
        Location location = locationService.add(name, img);
        return "redirect:/location/" + location.getLocationId() + "/edit/";
    }

    @PreAuthorize("hasAuthority('Role Sharing Editors')")
    @PostMapping("/cacheevict")
    public String cacheEvict(@RequestParam("locationId") long currentLocationId) {
        locationService.locationCacheEvict();
        return "redirect:/location/" + currentLocationId + "/edit/";
    }

    @PreAuthorize("hasAuthority('Role Sharing Editors')")
    @GetMapping("/{currentLocation}/edit/{place}")
    public String editPlace(@PathVariable Location currentLocation, @PathVariable Place place, Model model) {
        model.addAttribute("currentLocation", currentLocation);
        model.addAttribute("locationsList", locationService.getLocationsList());
        model.addAttribute("imagesDirectory", imagesDirectory);
        model.addAttribute("currentPlace", place);
        return "editplace";
    }

    @PreAuthorize("hasAuthority('Role Sharing Editors')")
    @PostMapping("/{currentLocation}/edit/{place}")
    public String editPlace(Place place) {
        placeService.editPlace(place);
        return "redirect:/location/" + place.getLocation().getLocationId() + "/edit";
    }


    @PreAuthorize("hasAuthority('Role Sharing Editors')")
    @PostMapping("/{currentLocation}/edit/{place}/delete")
    public String deletePlace(Place place) {
        placeService.deletePlace(place);
        return "redirect:/location/" + place.getLocation().getLocationId() + "/edit";
    }

}
