package ru.tele2.spacebook.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.tele2.spacebook.service.LocationService;

@Controller
@PreAuthorize("hasAuthority('Role Sharing Users')")
public class MainPageController {
    private final LocationService locationService;

    public MainPageController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("locationsList", locationService.getLocationsList());
        return "index";
    }
}
