package ru.tele2.spacebook.controller;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.tele2.spacebook.config.ldap.ExtendedLdapUserDetails;
import ru.tele2.spacebook.domain.Place;
import ru.tele2.spacebook.exception.MultipleReservationException;
import ru.tele2.spacebook.exception.PlaceReservedException;
import ru.tele2.spacebook.service.ReservationService;

import java.time.LocalDate;

@Controller
@PreAuthorize("hasAuthority('Role Sharing Users')")
public class ReservationController {
    private final ReservationService reservationService;

    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @PostMapping("/place/{place}/setreservation")
    public String setReservation(RedirectAttributes redirectAttributes,
                                 @PathVariable Place place,
                                 @RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                 @AuthenticationPrincipal ExtendedLdapUserDetails user) throws Exception {
        try {
            reservationService.setReservation(user, date, place);
            redirectAttributes.addFlashAttribute("message", "Место зарезервировано. " + "Ряд " + place.getRowNumber() +
                    " место " + place.getNumber());
        } catch (PlaceReservedException | MultipleReservationException e) {
            redirectAttributes.addFlashAttribute("message", e.getMessage());
        }
        return "redirect:/location/" + place.getLocation().getLocationId() + "/date/" + date;
    }

    @PostMapping("/place/{place}/cancelreservation")
    public String cancelReservation(RedirectAttributes redirectAttributes,
                                    @PathVariable Place place,
                                    @RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                    Authentication authentication) {
        ExtendedLdapUserDetails user = (ExtendedLdapUserDetails) authentication.getPrincipal();
        try {
            reservationService.cancelReservation(date, place, user);
            redirectAttributes.addFlashAttribute("message", "Резерв снят.");
        } catch (PlaceReservedException | MultipleReservationException e) {
            redirectAttributes.addFlashAttribute("message", e.getMessage());
        }
        return "redirect:/location/" + place.getLocation().getLocationId() + "/date/" + date;
    }

}
