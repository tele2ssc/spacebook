package ru.tele2.spacebook.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.tele2.spacebook.domain.Location;
import ru.tele2.spacebook.domain.Reservation;
import ru.tele2.spacebook.repository.LocationIdAndName;
import ru.tele2.spacebook.repository.LocationRepository;
import ru.tele2.spacebook.repository.ReservationRepository;
import ru.tele2.spacebook.service.LocationService;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class LocationServiceImpl implements LocationService {
    private final LocationRepository locationRepository;
    private final ReservationRepository reservationRepository;

    @Value("${spacebook.images.upload.directory}")
    private String uploadPath;

    @Autowired
    ServletContext context;

    @Autowired
    public LocationServiceImpl(LocationRepository locationRepository, ReservationRepository reservationRepository) {
        this.locationRepository = locationRepository;
        this.reservationRepository = reservationRepository;
    }

    @Cacheable(value = "LocationsList")
    public List<LocationIdAndName> getLocationsList() {
        return locationRepository.findAllBy();
    }

    @Override
    @Cacheable(value = "Location", key = "{#id, #date}")
    public Location getByIdAndReservationDate(long id, LocalDate date) {
        Location location = locationRepository.getByIdFetchPlaces(id);
        if (location == null) {
            location = locationRepository.getById(id);
        }
        Set<Reservation> reservations = reservationRepository.findByDateAndPlace_Location_LocationIdFetchPlaces(date, id);
        for (Reservation reservation : reservations) {
            location.getPlaces().get(location.getPlaces().indexOf(reservation.getPlace())).setReservation(reservation);
        }
        return location;
    }

    @Override
    @CacheEvict(value = "LocationsList", allEntries = true)
    public Location add(String name, MultipartFile img) throws IOException {
        Location location = new Location();
        if (!img.isEmpty()) {
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdirs();
            }
            if (!img.getOriginalFilename().isEmpty()) {
                String originalFileExtension = img.getOriginalFilename().substring(img.getOriginalFilename().lastIndexOf('.'));
                String resultFileName = UUID.randomUUID().toString() + originalFileExtension;
                img.transferTo(Paths.get(uploadDir + File.separator + resultFileName));
                location.setName(name);
                location.setBackgroundImagePath(resultFileName);
            }
        }
        return locationRepository.save(location);
    }

    @CacheEvict(value = "Location", allEntries = true)
    public void locationCacheEvict() {
    }
}
