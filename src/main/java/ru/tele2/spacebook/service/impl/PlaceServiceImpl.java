package ru.tele2.spacebook.service.impl;

import org.springframework.stereotype.Service;
import ru.tele2.spacebook.domain.Location;
import ru.tele2.spacebook.domain.Place;
import ru.tele2.spacebook.repository.PlaceRepository;
import ru.tele2.spacebook.service.LocationService;
import ru.tele2.spacebook.service.PlaceService;

import java.time.LocalDate;

@Service
public class PlaceServiceImpl implements PlaceService {
    private final PlaceRepository placeRepository;
    private final LocationService locationService;

    public PlaceServiceImpl(PlaceRepository placeRepository, LocationService locationService) {
        this.placeRepository = placeRepository;
        this.locationService = locationService;
    }

    @Override
    public Place addPlace(Place place) {
        placeRepository.save(place);
        return place;
    }

    @Override
    public Place editPlace(Place place) {
        placeRepository.save(place);
        return place;
    }

    @Override
    public void deletePlace(Place place) {
        placeRepository.delete(place);
    }

    public Place getCachedPlace(LocalDate date, Place place) {
        Location location = locationService.getByIdAndReservationDate(place.getLocation().getLocationId(), date);
        return  location.getPlaces().get(location.getPlaces().indexOf(place));
    }
}
