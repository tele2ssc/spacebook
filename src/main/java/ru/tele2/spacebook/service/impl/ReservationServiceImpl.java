package ru.tele2.spacebook.service.impl;

import org.springframework.stereotype.Service;
import ru.tele2.spacebook.config.ldap.ExtendedLdapUserDetails;
import ru.tele2.spacebook.domain.Place;
import ru.tele2.spacebook.domain.Reservation;
import ru.tele2.spacebook.exception.MultipleReservationException;
import ru.tele2.spacebook.exception.PlaceReservedException;
import ru.tele2.spacebook.repository.ReservationRepository;
import ru.tele2.spacebook.service.LocationService;
import ru.tele2.spacebook.service.PlaceService;
import ru.tele2.spacebook.service.ReservationService;

import javax.transaction.Transactional;
import java.time.LocalDate;

@Service
public class ReservationServiceImpl implements ReservationService {
    private final ReservationRepository reservationRepository;
    private final PlaceService placeService;

    public ReservationServiceImpl(ReservationRepository reservationRepository,
                                  PlaceService placeService) {
        this.reservationRepository = reservationRepository;
        this.placeService = placeService;
    }

    @Override
    @Transactional
    public void setReservation(ExtendedLdapUserDetails user, LocalDate date, Place place)
            throws PlaceReservedException, MultipleReservationException {
        Reservation reservation = new Reservation();
        reservation.setDate(date);
        reservation.setPlace(place);
        reservation.setSid(user.getSid());
        reservation.setDisplayName(user.getDisplayName());
        if (isReservationAvailable(reservation, user)) {
            if (reservationRepository.save(reservation) == reservation) {
                placeService.getCachedPlace(date, place).setReservation(reservation);
            }
        }
    }



    private boolean isReservationAvailable(Reservation reservation, ExtendedLdapUserDetails user)
            throws MultipleReservationException, PlaceReservedException {
        return !isPlaceReserved(reservation) && !isMultipleReservation(reservation, user);
    }

    private boolean isPlaceReserved(Reservation reservation) throws PlaceReservedException {
        Reservation existedReservation = placeService.getCachedPlace(reservation.getDate(), reservation.getPlace()).getReservation();
        if (existedReservation != null) {
            throw new PlaceReservedException("Выбранное место занято " + existedReservation.getDisplayName());
        } else
        return false;
    }

    private boolean isMultipleReservation(Reservation reservation,
                                          ExtendedLdapUserDetails user) throws MultipleReservationException {
        Reservation existedReservation = reservationRepository.findByDateAndSid(reservation.getDate(), user.getSid());
        if (existedReservation != null) throw new MultipleReservationException(
                "Множественное резервирование невозможно! Вы уже заняли Локация " +
                        existedReservation.getPlace().getLocation().getName() +
                        " ряд " + existedReservation.getPlace().getRowNumber() + " место " +
                        existedReservation.getPlace().getNumber());
        return false;
    }

    private boolean isReservationCancelAllowed(LocalDate date,
                                               Place place,
                                               ExtendedLdapUserDetails user) throws PlaceReservedException {
        Reservation reservation = placeService.getCachedPlace(date, place).getReservation();
        if (reservation != null && !reservation.getSid().equals(user.getSid())) {
            throw new PlaceReservedException("Выбранное место занято!");
        } else
            return true;
    }

    @Override
    @Transactional
    public void cancelReservation(LocalDate date, Place place, ExtendedLdapUserDetails user)
            throws PlaceReservedException {
        if (isReservationCancelAllowed(date, place, user)) {
            if (reservationRepository.deleteByDateAndPlace(date, place) > 0) {
                placeService.getCachedPlace(date, place).setReservation(null);
            }
        }
    }
}
