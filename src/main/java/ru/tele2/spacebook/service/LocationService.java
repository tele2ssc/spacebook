package ru.tele2.spacebook.service;

import org.springframework.web.multipart.MultipartFile;
import ru.tele2.spacebook.domain.Location;
import ru.tele2.spacebook.repository.LocationIdAndName;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public interface LocationService {
    List<LocationIdAndName> getLocationsList();
    Location getByIdAndReservationDate(long id, LocalDate date);
    Location add(String name, MultipartFile img) throws IOException;
    void locationCacheEvict();
}
