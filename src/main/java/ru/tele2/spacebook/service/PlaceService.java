package ru.tele2.spacebook.service;

import ru.tele2.spacebook.domain.Place;

import java.time.LocalDate;

public interface PlaceService {
    Place  addPlace(Place place);
    Place getCachedPlace(LocalDate date, Place place);
    Place editPlace(Place place);
    void deletePlace(Place place);
}
