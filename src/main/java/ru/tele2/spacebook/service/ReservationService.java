package ru.tele2.spacebook.service;

import ru.tele2.spacebook.config.ldap.ExtendedLdapUserDetails;
import ru.tele2.spacebook.domain.Place;
import ru.tele2.spacebook.exception.MultipleReservationException;
import ru.tele2.spacebook.exception.PlaceReservedException;

import java.time.LocalDate;

public interface ReservationService {
    void setReservation(ExtendedLdapUserDetails user, LocalDate date, Place place) throws PlaceReservedException, MultipleReservationException;
    void cancelReservation(LocalDate date, Place place, ExtendedLdapUserDetails user) throws PlaceReservedException, MultipleReservationException;
}
