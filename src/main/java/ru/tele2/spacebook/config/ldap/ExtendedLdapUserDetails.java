package ru.tele2.spacebook.config.ldap;

import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.support.LdapUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.ldap.userdetails.LdapUserDetails;

import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.Collection;

public class ExtendedLdapUserDetails implements LdapUserDetails {
    private String displayName;
    private String location;
    private String sid;
    private String dn;
    private String password;
    private String username;
    private Collection<GrantedAuthority> authorities;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;
    private int timeBeforeExpiration;
    private int graceLoginsRemaining;

    public ExtendedLdapUserDetails() {
        this.authorities = AuthorityUtils.NO_AUTHORITIES;
        this.accountNonExpired = true;
        this.accountNonLocked = true;
        this.credentialsNonExpired = true;
        this.enabled = true;
        this.timeBeforeExpiration = 2147483647;
        this.graceLoginsRemaining = 2147483647;
    }

    public ExtendedLdapUserDetails(DirContextOperations ctx, String username, Collection<? extends GrantedAuthority> authorities) {
        this();
        this.dn = ctx.getDn().toString();
        this.username = username;
        this.location = ctx.getStringAttribute("l");
        byte[] byteSid = null;
        try {
            byteSid = (byte[]) ctx.getAttributes().get("objectSid").get();
        } catch (NamingException e) {
            e.printStackTrace();
        }
        this.sid = LdapUtils.convertBinarySidToString(byteSid);
        this.displayName = ctx.getStringAttribute("displayName");
        this.authorities = new ArrayList<>();
        this.authorities.addAll(authorities);
    }

    public String getSid() {
        return sid;
    }

    public String getLocation() {
        return location;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public void eraseCredentials() {
        this.password = null;
    }

    @Override
    public String getDn() {
        return this.dn;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }
}
