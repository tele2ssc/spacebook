package ru.tele2.spacebook.config.ldap;

import org.springframework.security.core.AuthenticationException;

public class ActiveDirectoryAuthenticationException extends AuthenticationException {
    private final String dataCode;

    ActiveDirectoryAuthenticationException(String dataCode, String message, Throwable cause) {
        super(message, cause);
        this.dataCode = dataCode;
    }

    public String getDataCode() {
        return this.dataCode;
    }
}
