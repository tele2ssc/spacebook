package ru.tele2.spacebook.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;
import ru.tele2.spacebook.config.ldap.ExtendedLdapUserDetailsMapper;
import ru.tele2.spacebook.config.ldap.NestedGroupsActiveDirectoryAuthenticationProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Value("${ldap.url}")
    private String ldapUrl;
    @Value("${ldap.domain}")
    private String ldapDomain;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .anyRequest().fullyAuthenticated()
                .and()
                .headers().frameOptions().disable()
                .and()
                .formLogin();

    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(activeDirectoryLdapAuthenticationProvider());
    }

    @Bean
    public AuthenticationProvider activeDirectoryLdapAuthenticationProvider() {
        NestedGroupsActiveDirectoryAuthenticationProvider provider = new NestedGroupsActiveDirectoryAuthenticationProvider(ldapDomain, ldapUrl);
        provider.setSearchFilter("(&(objectClass=user)(sAMAccountName={1}))");
        provider.setConvertSubErrorCodesToExceptions(true);
        provider.setUseAuthenticationRequestCredentials(true);
        Map<String, Object> environmentProperties = new HashMap<>();
        environmentProperties.put("java.naming.ldap.attributes.binary", "objectsid");
        provider.setContextEnvironmentProperties(environmentProperties);
        provider.setUserDetailsContextMapper(userDetailsContextMapper());
        return provider;
    }

    @Bean
    public AuthenticationManager authenticationManager() {
        return new ProviderManager(Arrays.asList(activeDirectoryLdapAuthenticationProvider()));
    }

    @Bean
    public UserDetailsContextMapper userDetailsContextMapper() {
        return new ExtendedLdapUserDetailsMapper();
    }
}
