package ru.tele2.spacebook.exception;

public class PlaceReservedException extends Exception{
    public PlaceReservedException() {
        super();
    }

    public PlaceReservedException(String message) {
        super(message);
    }
}
