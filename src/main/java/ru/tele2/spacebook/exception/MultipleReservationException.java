package ru.tele2.spacebook.exception;

public class MultipleReservationException extends Exception {
    public MultipleReservationException() {
        super();
    }

    public MultipleReservationException(String message) {
        super(message);
    }
}
