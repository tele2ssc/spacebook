<#import "main.ftl" as main>
<@main.page>

<div class="content">
<div class="block">
    <img id="locationImg" src="${imagesDirectory}${currentLocation.backgroundImagePath}">
        <#list currentLocation.places as place>
	        <div class="place schema-seats" style="left: ${place.XPosition?c}px; top: ${place.YPosition?c}px;">
		        <div class="place-number" style="border-color: #64D49E;">
		            <a href="/location/${currentLocation.locationId?c}/edit/${place.placeId?c}"> ${place.number?c} </a>
		        </div>

			    <div class="place-content">
			         <span>Ряд ${place.rowNumber?c} Место ${place.number?c}</span>
			         <span>X = ${place.XPosition?c} Y = ${place.YPosition?c}</span>
			    </div>
		    </div>
		</#list>
   	</div>
<div>
    <form method="post" action="/location/addplace">
    <label for="inputrowNumber">Ряд</label>
    <input type="text" name="rowNumber" id="inputrowNumber" required autofocus>
    <label>Место</label>
    <input type="text" name="number" id="inputnumber" required>
    <label>Позиция по горизонтали</label>
    <input type="number" name="xPosition" id="inputXPos" required>
    <label>Позиция по вертикали</label>
    <input type="number" name="yPosition" id="inputYPos" required>
    <input type="hidden" name="location" value="${currentLocation.locationId}" required>
    <button type="submit">Добавить</button>
    </form>
</div>
<div>
    <form method="post" action="/location/cacheevict">
    <input type="hidden" name="locationId" value="${currentLocation.locationId}" required>
    <button type="submit">Обновить кэш</button>
    </form>
</div>
</div>
</@main.page>