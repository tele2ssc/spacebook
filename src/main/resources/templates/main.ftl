<#macro page>
<#include "security.ftl">
<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>SpaceBook - Book your seat</title>
  <link rel="stylesheet" href="/css/styles.css">

  <script>
    function controlsHandler(locationId, date){
      var url = locationId;
        if (!!date) url += "/date/" + date;

      document.location.assign(url);
    }

    window.onload = function() {
       var x = document.getElementById("snackbar");
       if (x != null) {
           x.className = "show";
           setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
       }
    };
  </script>
 </head>

 <body>
    <div class="header">
        <form id="headerForm" name="headerForm">
        <ul>
            <li><img src="/img/logo.jpg" alt="Tele2"></li>
            <li><span>Локация</span></li>
            <li>
                <select name="locationId" id="locationsDropDownList" onchange="controlsHandler(locationsDropDownList.value, date.value);">
                <#if !currentLocation?exists> <option value="" selected></option></#if>
                    <#if locationsList??>
                    <#list locationsList as location>
                     <option
                     <#if currentLocation?exists><#if currentLocation.locationId == location.locationId>selected</#if></#if>
                      value="/location/${location.locationId}">${location.name}</option>
                    </#list>
                    </#if>
	            </select></li>
            <li><span>Дата</span></li>
            <li><input type="date" name="date" onchange="controlsHandler(locationsDropDownList.value, date.value);"
                        <#if currentDate??> value="${currentDate}" <#else> disabled </#if> required></li>
            <li style="float:right">
                <#if isAuthenticated?exists>
                    <a href="/logout">${user.displayName}</a>
                <#else>
                    <a href="/login">Login</a>
                </#if>
            </li>
        </ul>
        </form>
    </div>
   	<#nested>
   	<#if message??>
   	    <div id="snackbar" >${message}</div>
   	</#if>
  </body>
</html>

</#macro>
