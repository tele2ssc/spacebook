<#import "main.ftl" as main>
<@main.page>
<div class="content">
<div class="block">
    <div>
        <form method="post" enctype="multipart/form-data" action="/location/add">
        <input type="text" name="name" id="inputName" placeholder="Локация" required autofocus>
        <input type="file" name="backgroundImagePath" id="fileBackgroundImagePath">
        <button type="submit">Добавить</button>
        </form>
    </div>
</@main.page>