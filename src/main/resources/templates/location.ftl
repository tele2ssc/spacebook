<#import "main.ftl" as main>
<@main.page>
<div class="content">
<div class="block">
    <img id="locationImg" src="${imagesDirectory}${currentLocation.backgroundImagePath}">
        <#if currentLocation.places??>
        <#list currentLocation.places as place>
            <button form="headerForm" formmethod="POST" style="left: ${place.XPosition?c}px; top: ${place.YPosition?c}px;" type="submit"
            <#if !place.reservation?exists>
            formaction="/place/${place.placeId?c}/setreservation" class="place schema-seats">
            	<div class="place-content">
            	    <span>Ряд ${place.rowNumber?c} Место ${place.number?c}</span>
            		<span>Свободно для бронирования</span>
            	</div>
            </button>
            <#else>
            formaction="/place/${place.placeId?c}/cancelreservation" class="place-reserved schema-seats" >
            <div class="place-content">
                <span>Ряд ${place.rowNumber?c} Место ${place.number?c}</span>
                <span>${place.reservation.displayName}</span>
            </div>
            </#if>
		</#list>
        </#if>
   	    </div>
   	</div>
</@main.page>